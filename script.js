var typed = new Typed(".animate", {
  strings: [
    "a Junior Developer",
    "a System's Analyst",
    "an Engineering Student",
  ],
  typeSpeed: 50,
  backspeed: 50,
  loop: 50,
});
